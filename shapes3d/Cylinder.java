package shapes3d;
import shapes2d.Circle;

public class Cylinder extends Circle {

	int height;
	
	public Cylinder(int radius, int height) {
		super(radius);
		this.height=height;
	}
	
	public double area() {
		return (2 * Math.PI * super.radius + height) + (2 * super.area());
	}
	
	public double volume() {
		return super.area()*height;
	}
	
	public String toString() {
		return "Height of the cylinder= " +height+", Radius of the cylinder= " +super.radius;
	}
}
